{pkgs, ...}: {
  plugins.neogit = {
    enable = true;
  };
  extraConfigLua = ''
    vim.keymap.set({'n'}, '<leader>gs', '<cmd>Neogit<cr>', {
      noremap = true,
      silent = true,
      desc = "[G]it [S]tatus",
    })
  '';
}
