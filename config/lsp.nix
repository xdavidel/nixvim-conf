{
  config.plugins.which-key.registrations = {
    "<leader>k" = "Diag Prev";
    "<leader>j" = "Diag Next";
    "<leader>ca" = "[C]ode [A]ction";
    "<leader>lr" = "[L]sp [R]ename";
    "<leader>wa" = "[W]orkspace [A]dd Folder";
    "<leader>wr" = "[W]orkspace [R]emove Folder";
  };
  config.plugins.lsp = {
    enable = true;
    keymaps = {
      silent = true;
      diagnostic = {
        "<leader>k" = "goto_prev";
        "<leader>j" = "goto_next";
      };
      lspBuf = {
        "gD" = "declaration";
        "<C-LeftMouse>" = "definition";
        "gt" = "type_definition";
        "K" = "hover";

        "<C-k>" = "signature_help";

        "<leader>ca" = "code_action";
        "<leader>lr" = "rename";
        "<leader>wa" = "add_workspace_folder";
        "<leader>wr" = "remove_workspace_folder";
      };
    };
    onAttach = ''
      vim.keymap.set({"n"}, "gr",  "<cmd>Telescope lsp_references<CR>", {desc="references"})
      vim.keymap.set({"n"}, "gd",  "<cmd>Telescope lsp_definitions<CR>", {desc="definition"})
      vim.keymap.set({"n"}, "gs",  "<cmd>lua require('telescope.builtin').lsp_workspace_symbols()<CR>", {desc="[G]o to [S]ymbol"})
      vim.keymap.set({"n"}, "gp",  "<cmd>Telescope diagnostics<CR>", {desc="[G]o to [P]roblems"})
      vim.keymap.set({"n"}, "gI",  "<cmd>Telescope lsp_implementations<CR>", {desc="[G]o to [I]mplementation"})

      vim.keymap.set({"n"}, "<C-t>",  "<cmd>lua require('telescope.builtin').lsp_document_symbols()<CR>", {desc="local symbols"})
      vim.keymap.set({"n"}, "<C-S-t>",  "<cmd>lua require('telescope.builtin').lsp_dynamic_workspace_symbols()<CR>", {desc="global symbols"})

      vim.keymap.set({"n"}, "<leader>lt",  "<cmd>Telescope lsp_type_definitions<CR>", {desc="[L]sp [T]ypes"})
      vim.keymap.set({"n"}, "<leader>lfi",  "<cmd>Telescope lsp_incoming_calls<CR>", {desc="[L]sp [F]unction [I]ncoming"})
      vim.keymap.set({"n"}, "<leader>lfo",  "<cmd>Telescope lsp_outgoing_calls<CR>", {desc="[L]sp [F]unction [O]utgoing"})
    '';

    servers = {
      bashls.enable = true;
      ccls.enable = true;
      gopls.enable = true;
      zls.enable = true;
      lua-ls = {
        enable = true;
        settings = {
          workspace.checkThirdParty = true;
          telemetry.enable = false;
        };
      };

      nil_ls.enable = true;
      pyright.enable = true;

      marksman.enable = true;

      rust-analyzer = {
        enable = true;
        installCargo = true;
        installRustc = true;
        settings = {diagnostics.enable = true;};
      };
    };
  };
}
