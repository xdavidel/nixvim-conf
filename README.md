# Nixvim configuration

This is a NixVim standalone configuration.

## Standalone run

1. Locally

```
nix run .
```

2. From repository

```sh
nix run gitlab:xdavidel/nixvim-conf
```
